<?php

use Drupal\setting_snapshot\Drush;

//  Can't use xautoload
function setting_snapshot_drush_command() {
  //require_once 'sites/all/modules/xautoload/xautoload.module';
  //xautoload_init();
  return class_exists('Drush') ? Drush::create()->export() : [];
}

//function drush_setting_snapshot_view() {
//  $drush = new Drush();
//  return $drush->view();
//}
