<?php
namespace Drupal\setting_snapshot;

class Snapshot {
  var $name = '';
  var $content = ['variable' => [], 'user_permission' => []];

  function __construct($name = NULL) {
    if (isset($name)) {
      $this->load($name);
    } else {
      $this->create();
    }
  }

  protected function load($name) {
    $content = file_get_contents(Core::getFilename($name));
    $data = drupal_json_decode(gzinflate($content));

    foreach (array_keys(get_object_vars($this)) as $item) {
      $this->content[$item] = isset($data[$item]) ? $data[$item] : [];
    }

    $this->name = $name;
  }

  protected function create() {
    $this->setVariable();
    $this->setUserPermission();
  }

  protected function setVariable() {
    $table = 'variable';
    $data = [];

    foreach (db_select($table)->fields($table)->execute()->fetchAllKeyed() as $index => $item) {
      $data[$index] = md5($item);
    }

    $this->content[$table] = $data;
  }

  protected function setUserPermission() {
    $table = 'role_permission';
    $data = [];

    foreach (db_select($table)->fields($table)->execute()->fetchAll() as $item) {
      $data[$item->permission][] = $item->rid;
    }

    $this->content['user_permission'] = $data;
  }

  function save($name) {
    $this->name = $name;
    $filename = Core::getFilename($name);
    $json = drupal_json_encode(get_object_vars($this));
    $data = gzdeflate($json, 9);
    return file_unmanaged_save_data($data, $filename, FILE_EXISTS_REPLACE);
  }

  function compare($name = NULL) {
    $data = [];
    $snapshot = new Snapshot($name);

    foreach ($snapshot->content as $index => $items) {
      $result = $this->differentKeys($items, $this->content[$index]);

      if (FALSE == empty($result)) {
        $data[$index] = $result;
      }
    }

    return $data;
  }

  protected function differentKeys(array $data1, array $data2) {
    $different = [];

    foreach ($data1 as $index => $item) {
      $current = &$data2[$index];

      if ((FALSE == isset($current)) || ($item != $current)) {
        $different[] = $index;
      }

      unset($data2[$index]);
    }

    return array_merge($different, array_keys($data2));
  }
}
