<?php
namespace Drupal\setting_snapshot;

use Drupal\mixin\Drush as Base;
use Drupal\mixin\Drush\Command;
use Drupal\mixin\ParseClass;

class Drush extends Base {

  function doView() {
    $files = Core::getFiles();

    if (empty($files)) {
      return drush_print(dt('No snapshot exist.'));
    }

    $rows = [];
    $rows[] = [
      dt('Name'),
      dt('Date'),
      dt('Size'),
    ];

    foreach ($files as $item) {
      $rows[] = [
        $item->name,
        format_date($item->filemtime),
        format_size($item->filesize),
      ];
    }

    return drush_print_table($rows, TRUE);
  }

  protected function doMapping() {
    $items = [
      'view' => 'doView'
    ];
    return $items;
  }

  protected function commandView() {
    $module = ParseClass::get(get_called_class(), 'module');
    $items = Command::create()
      ->setDescription('List all the available setting snapshot for your site.')
      ->setDrupalDependencies([$module])
      ->setAliases([$module])
      ->export($module . '-view');
    //$items[$this->command . '-view'] = [
    //  'description' => "List all the available setting snapshot for your site.",
    //  'drupal dependencies' => ['setting_snapshot'],
    //  'aliases' => ['setting_snapshot'],
    //];

    return $items;
  }
}
