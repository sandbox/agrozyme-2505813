<?php

namespace Drupal\setting_snapshot\Action;

use Drupal\setting_snapshot\Core;
use Drupal\setting_snapshot\Snapshot;

class Save extends Base {
  var $overwrite = '';

  protected function doForm() {
    drupal_set_title(t('Save setting snapshot'));

    /** @var Snapshot|false $snapshot */
    $snapshot = reset($this->arguments);
    $overwrite = $this->overwrite = ($snapshot) ? $snapshot->name : '';
    $form = parent::doForm();
    $form['#tree'] = TRUE;
    $form += $this->formSnapshot();

    if (('' === $overwrite) && empty($this->state['snapshot']['file_exists'])) {
      $form += $this->formActions();
    } else {
      $form = $this->confirm($form);
    }

    return $form;
  }

  protected function doSubmit() {
    $snapshot = new Snapshot();
    $filename = $this->values['snapshot']['filename'];
    $arguments = ['%name' => $filename];

    if ($snapshot->save($filename)) {
      drupal_set_message(t('Save snapshot %name success.', $arguments));
    } else {
      drupal_set_message(t('Save snapshot %name failure.', $arguments), 'error');
    }

    $this->state['redirect'] = Core::$basePath;
  }

  protected function doValidate() {
    parent::doValidate();
    $state = &$this->state;

    if (Core::hasSnapshot($this->values['snapshot']['filename'])) {
      $state['snapshot']['file_exists'] = TRUE;
      $state['rebuild'] = TRUE;
    }
  }

  static function validateValues(array $values) {
    if (FALSE == preg_match('/^[-_\.a-zA-Z0-9]+$/', $values['snapshot']['filename'])) {
      $message = t('Invalid filename. It must only contain alphanumeric characters, dots, dashes and underscores. Other characters, including spaces, are not allowed.');
      form_set_error('snapshot][filename', $message);
      return FALSE;
    }

    if (FALSE == empty($values['confirm'])) {
      return FALSE;
    }

    return TRUE;
  }

  protected function formSnapshot() {
    $form = [];
    $form['snapshot']['filename'] = [
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#autocomplete_path' => Core::$basePath . '/autocomplete',
      '#required' => TRUE,
      '#maxlength' => 128,
      '#description' => t('Allowed characters: a-z, 0-9, dashes ("-"), underscores ("_") and dots.'),
    ];

    return $form;
  }

  protected function formActions() {
    $form = [];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => t('Create'),
      ],
    ];
    return $form;
  }

  protected function confirm($form) {
    $overwrite = $this->overwrite;
    $state = &$this->state;
    $filename = ('' === $overwrite) ? $state['values']['snapshot']['filename'] : $overwrite;
    $form['snapshot']['filename'] += [
      '#disabled' => TRUE,
      '#default_value' => $filename,
    ];
    $arguments = ['%name' => $filename];
    $question = t('Are you sure you want to replace the existing %name snapshot?', $arguments);
    $description = t('A snapshot with the same name already exists and will be replaced. This action cannot be undone.');
    $options = ['path' => Core::$basePath, 'attributes' => ['class' => ['button']]];
    return confirm_form($form, $question, $options, $description);
  }
}
