<?php

namespace Drupal\setting_snapshot\Action;

use Drupal\setting_snapshot\Core;
use Drupal\setting_snapshot\Snapshot;

class Delete extends Base {
  protected function doForm() {
    drupal_set_title(t('Delete setting snapshot'));

    /** @var Snapshot|false $snapshot */
    $snapshot = reset($this->arguments);
    $name = $snapshot->name;
    $form = parent::doForm();
    $form += $this->formQuestion($name);
    $form += $this->formName($name);
    $options = ['path' => Core::$basePath, 'attributes' => ['class' => ['button']]];
    return confirm_form($form, t('Confirm'), $options, t('This action cannot be undone.'), t('Delete'));
  }

  protected function doSubmit() {
    $name = $this->values['name'];
    $arguments = ['%title' => $name];

    if (Core::hasSnapshot($name)) {
      $arguments['%result'] = unlink(Core::getFilename($name)) ? t('success') : t('failed');
      drupal_set_message(t('Delete snapshot %title is %result.', $arguments));
    } else {
      drupal_set_message(t('Snapshot %title do not exist.', $arguments), 'warning');
    }

    $this->state['redirect'] = Core::$basePath;
  }

  protected function formQuestion($filename) {
    $form = [];
    $form['question'] = [
      '#title' => 'Are you sure you want to delete the snapshot?',
      '#theme' => 'item_list',
      '#items' => [$filename],
    ];

    return $form;
  }

  protected function formName($filename) {
    $form = [];
    $form['name'] = [
      '#type' => 'value',
      '#value' => $filename,
    ];

    return $form;
  }
}
