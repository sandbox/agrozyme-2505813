<?php

namespace Drupal\setting_snapshot\Action;

use Drupal\mixin\Form;
use Drupal\setting_snapshot\Core;

abstract class Base extends Form {

  protected function prepare() {
    return $this->checkPrivateWrapper() && $this->checkDirectory();
  }

  protected function checkPrivateWrapper() {
    if (file_stream_wrapper_valid_scheme('private')) {
      return TRUE;
    }

    $url = url('admin/config/media/file-system', ['query' => drupal_get_destination()]);
    $arguments = ['@file-settings-url' => $url];
    $message = t('The <a href="@file-settings-url">private filesystem</a> must be configured in order to create or load snapshots.', $arguments);
    drupal_set_message($message, 'error');
    return FALSE;
  }

  protected function checkDirectory() {
    $directory = Core::getUri();

    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      return TRUE;
    }

    $arguments = ['%directory' => $directory];
    $message = t('The snapshot directory %directory could not be created.', $arguments);
    drupal_set_message($message, 'error');
    return FALSE;
  }

  protected function checkAll() {
    return $this->checkPrivateWrapper() && $this->checkDirectory();
  }

  protected function formBack() {
    $form = [];
    $form['back'] = [
      '#theme' => 'link',
      '#text' => t('Back'),
      '#path' => Core::$basePath,
      '#options' => [
        'html' => FALSE,
        'attributes' => ['class' => ['button']]
      ],
    ];
    return $form;
  }
}
