<?php
namespace Drupal\setting_snapshot\Action;

use Drupal\setting_snapshot\Core;

class View extends Base {

  protected function doForm() {
    drupal_set_title(t('Setting snapshot'));
    $files = Core::getFiles();
    $form = parent::doForm();
    $form += $this->formFilename($files);

    if (FALSE == empty($files)) {
      $form += $this->formSubmit();
    }

    return $form;
  }

  protected function doSubmit() {
    $values = $this->values;
    $this->state['redirect'] = Core::$basePath . '/compare/' . $values['filename'];
  }

  protected function formFilename(array $files) {
    $form = [];
    $form['filename'] = [
      '#type' => 'tableselect',
      '#multiple' => FALSE,
      '#header' => $this->header(),
      '#options' => $this->options($files),
      '#empty' => t('No snapshot available.'),
    ];
    return $form;
  }

  protected function formSubmit() {
    $form = [];
    $form['compare'] = [
      '#type' => 'submit',
      '#value' => t('Compare'),
    ];
    return $form;
  }

  protected function header() {
    $header = [
        'name' => t('Name'),
        'date' => t('Date'),
        'size' => t('Size'),
      ] + $this->actions();

    return $header;
  }

  protected function options(array $files) {
    $rows = [];

    foreach ($files as $index => $item) {
      $name = $item->name;

      $rows[$name] = [
          'name' => $name,
          'date' => format_date($item->filemtime),
          'size' => format_size($item->filesize),
        ] + $this->actionLinks($name);
    }

    return $rows;
  }

  protected function actions() {
    $items = [
      'save' => t('Save'),
      'compare' => t('Compare'),
      'export' => t('Export'),
      'delete' => t('Delete'),
    ];

    return $items;
  }

  protected function actionLinks($name) {
    $items = [];

    foreach ($this->actions() as $index => $item) {
      $items[$index] = l($item, Core::$basePath . '/' . $index . '/' . $name);
    }

    return $items;
  }
}
