<?php

namespace Drupal\setting_snapshot\Action;

class Compare extends Base {
  protected function doForm() {
    drupal_set_title(t('Compare setting snapshot'));

    /** @var Snapshot|false $snapshot */
    $snapshot = reset($this->arguments);
    $form = parent::doForm();
    $data = $snapshot->compare();

    if (empty($data)) {
      drupal_set_message(t('The snapshot is same as current.'));
    } else {
      $form['#tree'] = TRUE;
      $form += $this->formView($data);
    }

    $form += $this->formBack();
    return $form;
  }

  protected function formView(array $data) {
    $form = [];

    foreach ($data as $group => $items) {
      $form['view'][$group] = [
        '#type' => 'fieldset',
        '#title' => $group,
        '#collapsible' => TRUE,
        'data' => [
          '#type' => 'textarea',
          '#disabled' => TRUE,
          '#value' => implode("\n", $items),
        ],
      ];
    }

    return $form;
  }
}
