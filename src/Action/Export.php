<?php

namespace Drupal\setting_snapshot\Action;

use Drupal\setting_snapshot\Core;
use Drupal\setting_snapshot\Feature;

class Export extends Base {

  protected function doForm() {
    drupal_set_title(t('Export setting snapshot'));
    $snapshot = reset($this->arguments);
    $form = parent::doForm();
    $data = $snapshot->compare();

    if (empty($data)) {
      drupal_set_message(t('The snapshot is same as current.'));
    } else {
      $form['#tree'] = TRUE;
      $form += $this->formFeature();
      $form += $this->formCompare($data);

      if (isset($form['feature'])) {
        $form += $this->formExport($form);
      }
    }

    $form += $this->formBack();
    return $form;
  }

  protected function doSubmit() {
    module_load_include('inc', 'features', 'features.export');
    features_include();
    $values = $this->values;
    $feature = new Feature($values['feature']);

    if ($feature->export($values['compare'])) {
      drupal_set_message(t('Export %name success.', ['%name' => $feature->setting->info['name']]));
    }

    $this->state['redirect'] = Core::$basePath;
  }

  protected function formFeature() {
    $form = [];
    $options = [];

    foreach (features_get_features(NULL, TRUE) as $index => $item) {
      $options[$index] = $item->info['name'];
    }

    if (FALSE == empty($options)) {
      $form['feature'] = [
        '#type' => 'select',
        '#title' => t('Feature'),
        '#required' => TRUE,
        '#options' => $options,
      ];
    }

    return $form;
  }

  protected function formCompare(array $data) {
    $form = ['compare' => []];

    foreach ($data as $group => $items) {
      $form['compare'][$group] = [
        '#type' => 'tableselect',
        '#header' => [
          'group' => t('Group'),
          'name' => t('Name'),
        ],
        '#options' => $this->options($group, $items),
        '#default_value' => drupal_map_assoc($items, $items),
        '#empty' => t('No snapshot available.'),
      ];
    }

    return $form;
  }

  protected function formExport($form) {
    $form = [];
    $form['export'] = [
      '#type' => 'submit',
      '#value' => t('Export'),
    ];

    return $form;
  }

  protected function options($group, array $data) {
    $options = [];

    foreach ($data as $name) {
      $options[$name] = [
        'group' => $group,
        'name' => $name,
      ];
    }

    return $options;
  }
}
