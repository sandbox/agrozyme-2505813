<?php
namespace Drupal\setting_snapshot;

class Feature {
  var $setting = [];

  function __construct($name) {
    $this->setting = features_get_features($name, TRUE);
  }

  function export(array $compare) {
    $setting = $this->setting;

    foreach ($this->compare($compare) as $index => $item) {
      $setting->info['features'][$index] = $item;
    }

    $result = TRUE;
    $result &= $this->saveInfo($setting);
    $result &= $this->saveStrongarm($setting);
    $result &= $this->saveUserPermission($setting);
    return $result;
  }

  function compare(array $compare) {
    $setting = $this->setting->info['features'];
    $result = [];

    foreach ($compare as $index => $item) {
      $data = (isset($setting[$index])) ? $setting[$index] : [];
      $result[$index] = $this->merge($data, $this->filter($item));
    }

    return $result;
  }

  protected function saveInfo($setting) {
    $content = features_export_info($setting->info);
    $filename = drupal_dirname($setting->filename) . '/' . $setting->name . '.info';
    return $this->save($content, $filename);
  }

  protected function saveStrongarm($feature) {
    $module = $feature->name;
    $name = 'strongarm';
    $setting = isset($feature->info['features']['variable']) ? $feature->info['features']['variable'] : [];
    $render = variable_features_export_render($module, $setting);
    $content = $this->exportCode($module, $name, $render);
    $filename = drupal_dirname($feature->filename) . '/' . $module . '.' . $name . '.inc';
    return $this->save($content, $filename);
  }

  protected function saveUserPermission($feature) {
    $module = $feature->name;
    $name = 'user_permission';
    $setting = isset($feature->info['features'][$name]) ? $feature->info['features'][$name] : [];
    $render = user_permission_features_export_render($module, $setting);
    $content = $this->exportCode($module, $name, $render);
    $filename = drupal_dirname($feature->filename) . '/' . $module . '.features.' . $name . '.inc';
    return $this->save($content, $filename);
  }

  protected function merge(array $setting, array $compare) {
    return array_keys(drupal_map_assoc($setting) + $compare);
  }

  protected function filter(array $data) {
    $result = [];

    foreach ($data as $index => $item) {
      if ($item === $index) {
        $result[$index] = $index;
      }
    }

    return $result;
  }

  protected function save($content, $filename) {
    $result = (bool)file_unmanaged_save_data($content, $filename, FILE_EXISTS_REPLACE);

    if (FALSE == $result) {
      drupal_set_message(t('Export %name failure.', ['%name' => $filename]), 'error');
    }

    return $result;
  }

  protected function exportCode($module_name, $filename, array $render) {
    $suffix = key($render);
    $contents = [
      "function {$module_name}_{$suffix}() {",
      reset($render),
      '}'
    ];

    return "<?php\n/**\n * @file\n * {$module_name}.{$filename}.inc\n */\n\n" . implode("\n", $contents) . "\n";
  }
}
