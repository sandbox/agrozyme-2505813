<?php

namespace Drupal\setting_snapshot;

class Core {
  static $basePath = 'admin/structure/features/setting_snapshot';
  static $extensionName = 'json.deflate';

  static function loadSnapshot($name) {
    return static::hasSnapshot($name) ? new Snapshot($name) : FALSE;
  }

  static function hasSnapshot($name) {
    $filename = static::getFilename($name);
    return file_exists($filename);
  }

  static function getFilename($name) {
    return static::getUri() . '/' . $name . '.' . static::$extensionName;
  }

  static function getUri() {
    return 'private://' . variable_get('configuration_feature_path', 'configuration_feature');
  }

  static function matchFile($text = '') {
    if ('' === $text) {
      return [];
    }

    $matches = [];

    foreach (static::getFiles($text) as $file) {
      $name = $file->name;
      $matches[$name] = check_plain($name);
    }

    return $matches;
  }

  static function getFiles($text = '') {
    $path = Core::getUri();
    file_create_htaccess($path, TRUE);
    $files = file_scan_directory($path, static::getFileMask($text));
    $break = drupal_strlen(static::$extensionName) + 1;

    foreach ($files as $index => $item) {
      $filename = $item->filename;
      $length = drupal_strlen($filename) - $break;
      $item->name = substr($filename, 0, $length);
      $item->filemtime = filemtime($index);
      $item->filesize = filesize($index);
      $files[$index] = $item;
    }

    return $files;
  }

  static function getFileMask($text = '') {
    $mask = '\.' . static::$extensionName . '$';
    $mask = ('' == $text) ? $mask : '/' . preg_quote($text) . '.*' . $mask;
    return '/' . $mask . '/';
  }
}
