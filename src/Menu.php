<?php

namespace Drupal\setting_snapshot;

use Drupal\mixin\Menu as Base;
use Drupal\mixin\Menu\Router;

class Menu extends Base {
  const POSITION = 5;

  protected function menuAction() {
    $menu = [];
    $base = Core::$basePath;

    $menu += $this->defaultRouter()
      ->setTitle('Setting snapshot')->setPageArguments(['setting_snapshot_view_form'])->export($base);

    $menu += Router::create()->setTitle('View')->setType(MENU_DEFAULT_LOCAL_TASK)->export($base . '/view');

    $menu += $this->defaultRouter()
      ->setTitle('Save')->setPageArguments(['setting_snapshot_save_form'])->export($base . '/save');

    $menu += $this->buildActionRouter('Save snapshot', 'save');
    $menu += $this->buildActionRouter('Delete snapshot', 'delete');
    $menu += $this->buildActionRouter('Compare snapshot', 'compare');
    $menu += $this->buildActionRouter('Export snapshot', 'export');
    return $menu;
  }

  protected function defaultRouter() {
    return Router::create()
      ->setType(MENU_LOCAL_TASK)
      ->setPageArguments('drupal_get_form')
      ->setAccessCallback('user_access')
      ->setAccessArguments(['access setting snapshot']);
  }

  protected function buildActionRouter($title, $name) {
    return $this->defaultRouter()
      ->setTitle($title)
      ->setType(MENU_VISIBLE_IN_BREADCRUMB)
      ->setPageArguments(['setting_snapshot_' . $name . '_form', static::POSITION])
      ->export(Core::$basePath . '/' . $name . '/%setting_snapshot');
  }

  protected function menuAutocomplete() {
    $menu = [];
    $menu += $this->defaultRouter()
      ->setType(MENU_CALLBACK)
      ->setPageCallback('setting_snapshot_autocomplete')->setPageArguments([
        'setting_snapshot_export_form',
        static::POSITION
      ])->export(Core::$basePath . '/autocomplete');

    return $menu;
  }
}
